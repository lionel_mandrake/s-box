﻿module descentState;

/**
 * 
 * classes storing s-boxes, s-boxes statistics, and the current state of the optimization.
 * 
 * Copyright (c) 2014. For any information write to drs.mandrake@gmail.com.
 * 
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.*/


import std.random;
import std.stdio;
import std.container;
import std.stream;
import std.algorithm;
import std.math;

import computations;
import knownValues;
import utils;

///S-box size
const uint sbox_logsize      = 8;
const uint sbox_size         = 1 << sbox_logsize;
const uint sbox_mask         = sbox_size - 1;
const uint transform_logsize = 2*sbox_logsize;
const uint transform_size    = 1 << transform_logsize;
const uint num_pairs         = (sbox_size*(sbox_size - 1)) >> 1;

struct sbox_stats
{
  int max, dmax;
  uint mult, dmult;

  // the score function should be as continuous as possible during descent
  // because large gaps are impossible to climb again, but it should have no zone where it is 
  // insensitive to the sbox parameters.

  // the ordering induced by this score function is not entirely satisfactory as it does not coincide
  // with the lexicagraphical order induced by the quadruplet (max, dmax, mult, dmult). 
  // therefore the best s-boxes may exactly coincide with the smallest ones for this order.


  double score()
  {
    double maxd = cast(double)max;
    double dmaxd = cast(double)dmax;
    double multd = cast(double)mult;
    double dmultd = cast(double)dmult;
    static if(false)
    {
      double cst = max >= 26 ? 250 : 25;
      double s  =  maxd/2 - exp(-multd/cst) + dmaxd - exp(-dmultd/250.);
    }
    else
    {
      double s  =  (maxd/2 - exp(-multd/50.))/5. + dmaxd - exp(-dmultd/250.);
    }
    return s*1000.;
  }
}

struct sbox_desc{
  uint[] sbox;
  sbox_stats stats;
  ulong[] hashfactors;

  this(int[] ha, int[] da, ulong[] hashfactors_e, )
  {
    sbox = new uint[sbox_size];
    hashfactors = hashfactors_e;
    uint count;
    do{
      auto rnd = Random (unpredictableSeed);
      draw_permutation(sbox, rnd);
      count = num_fixed_points(sbox);
      assert(is_permutation(sbox));
    } 
    while(count > 0);

    init_stats(ha, da);
  }

  this(uint[] x, int[] ha, int[] da, ulong[] hashfactors_e)
  {
    sbox = new uint[sbox_size];
    hashfactors = hashfactors_e;
    sbox[] = x[];
    init_stats(ha,da);
  }

  this(this)
  {
    sbox = sbox.dup;
  }

  ~this()
  {
    delete sbox;
  }

  double score()
  {
    return stats.score();
  }

  void init_stats(int[] ha, int[] da)
  {
    compute_maxlin2(stats.max,   stats.mult, sbox, ha);
    compute_maxdiff(stats.dmax, stats.dmult, sbox, da);
  }

  void update(uint a, uint b, int ha1[], int ha2[], int da1[], int da2[])
  {
    swap(sbox[a], sbox [b]);
    hadamard_update_transform(a, b, sbox, ha1, ha2);
    compute_maxs(stats.max, stats.mult, ha2);
    update_maxdiff(sbox, da1, da2, a, b);
    compute_maxs_for_diff(stats.dmax, stats.dmult, da2);
  }

  ulong hash()
  {
    ulong val;
    for(uint i = 0; i < sbox_size; i++)
    {
      val += (cast(ulong)sbox[i])*hashfactors[i];
    }
    return val;
  }

  long hashDelta(uint a, uint b)
  {
    long d1 = hashfactors[a]-hashfactors[b];
    long d2 = sbox[b] - sbox[a];
    return d1*d2;
  }
}

class descent_state
{
  uint iteration = 0;
  uint tried_pairs = 0;
  uint pair_index;
  bool order = true;
  double best = int.max;
  sbox_desc curr;
  sbox_stats prev;
  int[] ha1;
  int[] ha2;
  int[] da1;
  int[] da2;
  ulong[] hashfactors;
  Random rnd;

  this()
  {
    ha1 = new int[transform_size];
    ha2 = new int[transform_size];
    da1 = new int[transform_size];
    da2 = new int[transform_size];
    hashfactors = new ulong[sbox_size];
    rnd = Random(37);
    uint j = 0;
    for(uint i = 0; i < sbox_size; i++)
    {
      j += sbox_size - i - 1;
      hashfactors[i] = uniform(0,1L<<48, rnd);
    }
    curr = sbox_desc(ha1, da1, hashfactors);
  }
  
  ~this()
  {
    delete ha1;
    delete ha2;
    delete da1;
    delete da2;
    delete hashfactors;
  }

  
  void print_state(string prefix, Stream file)
  {
    file.writefln("%slinear approx: max bias: %d/%d ; multiplicity: %d", prefix , 
                  curr.stats.max, sbox_size, curr.stats.mult);
    file.writefln("%sdifferentials : max pairs with fixed difference: %d ; multiplicity: %d", 
                  prefix, curr.stats.dmax, curr.stats.dmult);
    uint f = num_fixed_points(curr.sbox);
    if(f > 0) file.writefln("%snum fixed points: %d", prefix, f);

  }

  void update_sbox_properties(uint a, uint b)
  {
    prev = curr.stats;
    if(order) 
    {
      curr.update(a, b, ha1, ha2, da1, da2);
    }
    else 
    {
      curr.update(a,b, ha2, ha1, da2, da1);
    }
  }

  void restore_sbox_properties(uint a, uint b)
  {
    swap(curr.sbox[a], curr.sbox[b]);
    curr.stats = prev;
  }
}

