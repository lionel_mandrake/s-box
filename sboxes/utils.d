﻿module utils;

/**
 *  Copyright (c) 2014. For any information write to drs.mandrake@gmail.com.
 * 
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>. */

import std.random;

void draw_permutation(uint[] ar, ref Random rnd)
{
  for (uint i = 0; i < ar.length; i++)
  {
    uint idx = cast(uint)uniform(0, i + 1, rnd); // draw idx in [0,i]
    ar[i] = ar[idx]; // right hand side initialized except if i == idx. 
    // In that case the line does nothing, and next line initializes ar[i] properly.
    ar[idx] = i;
  }
}

/**
 * Checks that a an array is a permutation of its indexes.
 */
bool is_permutation(uint ar[]) 
{
  uint[] count = new uint[ar.length];
  bool ans = true;
  for (int i = 0; i < ar.length; i++) 
  {
    count[i] = 0;
  }
  for (int i = 0; i < ar.length; i++) 
  {
    count[ar[i]]++;
  }
  for (int i = 0; i < ar.length; i++) 
  {
    if (count[i] <> 1) 
    {
      ans = false;
      break;
    }
  }
  return(ans);
}
