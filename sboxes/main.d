﻿module sbox;

/**
 * This program generates random bijective 8-bit S-boxes for symmetric block ciphers.
 * The generated S-boxes have good differential and linear properties, typically:
 * max linear bias 24/256 (AES / Camellia 16/256)
 * max number of differential pairs with same difference: 3 (AES/camellia: 2)
 * the presence of quadratic equations involving input and output bits and 
 * the degree of output bits as functions of input bits are also checked.
 * parameters influencing the program:
 *  score metric (in sbox_stats.score() descent_state.d). Lower scores are better.
 *  K0, a value s.t. one goes from a s-box with score s to one with score s'
 *   with probability min(K0^(s-s'),1)
 *  maxMapSize, the maximum size of a cache to remember s-box scores. 
 *   Each cache record uses approx. 80 bytes.
 * log_filename, the file where every best value (i.e. s-box with lowest score)
 *  is saved together with its characteristics.
 *
 * see known_values.d for examples of generated tables.
 * 
 * Copyright (c) 2014. For any information write to drs.mandrake@gmail.com.
 * 
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>. 
 */

import std.stdio;
import std.random;
import std.datetime;
import std.cstream;
import std.algorithm; // for swap
import std.math; // for pow
import std.container; //for redblacktree

import descentState;
import computations;
import utils;

immutable string log_filename = "results.txt";
immutable double K0 = .5;
immutable ulong maxMapSize = 0x100000; // corresponds to max. memory usage of approx. 80MB

class indexAndHash
{
  ulong index;
  ulong hash;
  
  this(ulong p_index, ulong p_hash)
  {
    index = p_index;
    hash = p_hash;
  }
  
  override int opCmp(Object o)
  {
    indexAndHash s = cast(indexAndHash) o;
    if(index < s.index) return -1;
    else if(index == s.index) return 0;
    else return 1;
  }
}

int main(char[][] args) 
{
  uint i, a, b, tmp;
  double K = K0;
  descent_state s = new descent_state;
  s.best = s.curr.score;
  bool lost, found_better;
  Stream file;  
  bool found_better_in_cycle;

  if(std.file.exists(log_filename)) std.file.remove(log_filename);
  file = new std.stream.File(log_filename, FileMode.Out);

  writeln("Initial S-box characteristics:");
  s.print_state("", std.cstream.dout);
  print_sbox_details(&s, file);
  writefln("Score: %f\n", s.curr.score());

  auto rnd = Random(unpredictableSeed);
  int a_start = 0, b_start = 1;
  a = a_start;
  b = b_start;
  double minDeltaScore = int.max;
  while (true) 
  {
    ulong refHash = s.curr.hash();
    double refScore = s.curr.score();
    do
    {
      if(b < sbox_size - 1) b++;
      else if(a < b - 1) 
      {
        a++;
        b = a + 1;
      }
      else 
      {
        a = 0;
        b = 1;
      }
      if(a == a_start && b == b_start)
      {
        // all pairs were tried:
        K = sqrt(K);
        writefln("All pairs tried; minimum score increase = %f", minDeltaScore);
        writefln("increasing K to ease constraints. new K = %f", K);
      }
    }
    while(s.curr.sbox[a] == b || s.curr.sbox[b] == a);
    s.tried_pairs++;

    double score;
    s.update_sbox_properties(a, b);
    score = s.curr.score();
    minDeltaScore = min(minDeltaScore, score-refScore);
    if( score < refScore  || 
       (score >= refScore && uniform!(uint)(rnd) < pow(K,score - refScore)*uint.max))
    {
      K = K0;
      s.order = !s.order;
      writefln("New S-box found in %d iterations, hash %x. Score / best: %f / %f", 
               s.tried_pairs, s.curr.hash(), s.curr.score(), s.best);
      s.tried_pairs = 0;
      minDeltaScore = int.max;
      a_start = uniform(0,sbox_size, rnd);
      b_start = uniform(0,sbox_size-1, rnd);
      if(b_start >= a_start) b_start++;
      else swap(a_start, b_start);
      a = a_start;
      b = b_start;
      assert(a_start < b_start);
      s.print_state("", std.cstream.dout);
      if(score < s.best)
      {
        writeln("Beats best box found so far\n");
        print_sbox_details(&s, file);
        s.best = s.curr.score();
      }
    }
    else
    {
      s.restore_sbox_properties(a, b);
    }
    s.iteration++;
  }
  delete(s);
  file.close();
  return 0;
}


void print_sbox_details(descent_state *s, Stream file)
{
  uint j;
  int degrees[sbox_logsize];
  bool exists_quadratic_equations;
  output_degrees(degrees,s.curr.sbox);
  exists_quadratic_equations = quadratic_equations(s.curr.sbox);
  file.writefln("// iteration %d",s.iteration);
  file.writefln("// score: %f", s.curr.score());
  file.writefln("// is bijective: %d", is_permutation(s.curr.sbox));
  s.print_state("// ", file);
  file.writef("// output degrees: ");
  for (j = 0; j < sbox_logsize; j++) file.writef("%d ", degrees[j]);
  file.writef("\n");
  file.writef("// quadratic equations: ");
  if (exists_quadratic_equations) file.writef("yes\n");
  else file.writef("no\n");

  uint[] cycle_sizes = new uint[sbox_size];
  bool[] used = new bool[sbox_size];// initialized to false
  uint curr_cycle = 0;
  uint total_values = 0;

  while(total_values < sbox_size)
  {
    uint start_val = 0;
    while(used[start_val]) start_val++;
    assert(start_val < sbox_size);
    uint val = start_val;
    do {
      used[val] = true;
      total_values++;
      val = s.curr.sbox[val];
      cycle_sizes[curr_cycle]++;
    }
    while(val != start_val);
    curr_cycle++; 
  }
  file.writefln("// %d cycles:", curr_cycle);
  file.writef("// ");
  for(uint i=0; i < curr_cycle; i++) file.writef("%d ", cycle_sizes[i]);
  file.writef("\n");
  file.writef("uint[%d] sbox_%d = [",sbox_size,s.iteration);
  for(j = 0; j < sbox_size; j++) 
  {
    if(j%16 == 0) file.writef("\n");
    file.writef("%#4x",s.curr.sbox[j]);
    if(j + 1 < sbox_size) file.writef(",");
  }
  file.writef("];\n\n");
}
