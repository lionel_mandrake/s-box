﻿module computations;

/** 
 * Functions computing the differential and linear properties of a S-box.
 * 
 * Copyright (c) 2014. For any information write to drs.mandrake@gmail.com.
 * 
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

import std.stdio;
import std.random;

import descentState;

/**
 * Computes the best linear approximation(s) of the sbox data[], and the number
 * of linear masks that reach it.
 *
 * Computation is performed with the help the walsh-hadamard transform function
 * hadamard_tranform(). max is the maximum bias of a linear form (y,z) on the sbox
 * i.e. 1/2 x |{x | z(sbox[x]) = y(x)}| - |{x | z(sbox[x]) != y(x)}|
 * and mult is the number of linear forms that have the maximum bias.*/
 

void compute_maxlin(out int max, out uint mult, uint sbox[], int hadamard_array[]) 
{
  uint compute_hamming(uint i) 
  {
    uint res=0;
    while (i!=0) 
    {
      if (i&1) res++;
      i >>= 1;
    }
    return(res);
  }

  void hadamard_transform (int v[]) 
  {
    uint expi, i, j, k;
    for (i = 0; i < sbox_logsize; i++) 
    {
      expi = 1 << i;
      for (j = 0; j < sbox_size; j += (expi << 1)) 
      {
        for (k = 0; k < expi; k++) 
        {
          int a = v[j + k];
          int b = v[j + k + expi];
          v[j + k] = a + b;
          v[j + k + expi] = a - b;
        }
      }
    }
  }
  
  void init_hadamard_array(int array[], uint sbox[]) 
  {
    for (uint i = 0; i < sbox_size; i++)
    {
      for (uint j = 0; j < sbox_size; j++)
      {
        array[i|j << sbox_logsize] = (compute_hamming(sbox[i]&j))&1 ? -1: 1;
      }
    }
  }
  
  init_hadamard_array(hadamard_array, sbox);
  for (uint j = 0; j < sbox_size; j++)
  {
    hadamard_transform(hadamard_array[j<<sbox_logsize .. (j+1)<<sbox_logsize]);
  }
  compute_maxs(max, mult, hadamard_array);
}

/**
 * Computes the best linear approximation(s) of the sbox data[], and the number
 * of linear masks that reach it, alternative implementation
 */

void compute_maxlin2(out int max, out uint mult, uint sbox[], int hadamard_array[]) 
{
  /*
   * In-place Walsh-Hadamard transform of its input.
   *
   * Performs the WH transform on the table array[], in-place.
   * The size of the table is the global parameter transform_size.
   */
  void hadamard_transform (int v[]) 
  {
    uint expi, i, j, k;
    for (i = 0; i < transform_logsize; i++) 
    {
      expi = 1 << i;
      for (j = 0; j < transform_size; j += (expi << 1)) 
      {
        for (k = 0; k < expi; k++) 
        {
          int a = v[j + k];
          int b = v[j + k + expi];
          v[j + k] = a + b;
          v[j + k + expi] = a - b;
        }
      }
    }
  }
  
  void init_hadamard_array(int array[], uint sbox[]) 
  {
    uint i;
    array[] = -1;
    for (i = 0; i < sbox_size; i++)
    {
      array[i|((sbox[i]) << sbox_logsize)] = 1;
    }
  }
  
  init_hadamard_array(hadamard_array, sbox);
  hadamard_transform(hadamard_array);
  hadamard_array[] /= 2;
  compute_maxs(max, mult, hadamard_array);
}


/* update hadamard transform of sbox previously computed with compute_maxlin2
 * after the sbox  values in position a and b were exchanged 
 * initial transform is in ha1
 * updated transform is in ha2
 * */
void hadamard_update_transform(uint a, uint b, uint sbox[], int ha1[], int ha2[]) 
{
  void hadamard_delta_transform(uint p1, uint p2, uint m1, uint m2, int ha1[], int ha2[]) 
  {
    static bool init=false;
    uint i;
    static uint[] hamming;
    uint compute_hamming(int i) 
    {
      uint res=0;
      while (i!=0) 
      {
        if (i&1) res++;
        i >>= 1;
      }
      return(res);
    }
    int contrib2(int i) 
    {
      return((hamming[i&m1]+hamming[i&m2]-hamming[i&p1]-hamming[i&p2]));
    }
    if (!init) {
      hamming = new uint[transform_size];
      for (i = 0; i < transform_size; i++)
      {
        hamming[i]=compute_hamming(i)&1;
      }
      init = true;
    }
    for (i = 0; i < transform_size; i++) 
    {
      ha2[i] = ha1[i] + ((contrib2(i)) << 1);
    }
  }
  
  hadamard_delta_transform(
    a|(sbox[a]<<sbox_logsize), b|(sbox[b]<<sbox_logsize), 
    b|(sbox[a]<<sbox_logsize), a|(sbox[b]<<sbox_logsize), ha1, ha2);
}

/** computes the largest bias of linear characteristics amd the number of linear 
 * characteristics with bias equal to the largest bias 
 * The largest bias is expressed as |#f(x,sbox[x])=1 - #f(x,sbox[x])=0|/2 */

void compute_maxs(out int max, out uint mult, int ha[]) 
{
  max = 0;
  mult = 0;
  uint i;
  int tmp;
  for (i = 1 ; i < transform_size; i++)
  {
    tmp = ha[i] >= 0 ? ha[i] : -ha[i];
    if (tmp > max) 
    {
      max = tmp;
      mult = 1;
    } 
    else if (tmp == max)
    {
      mult++;
    }
  }
  max >>= 1;
}

/**
 * Computes the number of differential(s) that are satisfied with highest
 * count and their count.
 *
 * max is the number of (ordered) pairs of messages that satisfy the best differential(s), 
 * and mult is the number of differentials that are satisfied for max pairs.
 */
void compute_maxdiff(out int max, out uint mult, uint sbox[], int da1[]) 
{
  uint i, j;
  da1[] = 0;
  for (i = 0; i < sbox_size; i++) 
  {
    for (j = i + 1; j < sbox_size; j++) 
    {
      da1[(i^j) | ((sbox[i]^sbox[j]) << sbox_logsize)]++;
    }
  }
  compute_maxs_for_diff(max, mult, da1);
}


/* updates difference table to account for the swapping of sbox[a] and sbox[b]. 
   assumes sbox[a] and sbox[b] were already swapped in the table */
void update_maxdiff(uint sbox[], int da1[], int da2[], uint a, uint b){
  uint i;
  da2[] = da1[];
  for(i = 0; i < sbox_size; i++)
  {
    if(i == a || i == b) continue;
    da2[(i^a) | ((sbox[i]^sbox[b])<<sbox_logsize)]--;
    da2[(i^a) | ((sbox[i]^sbox[a])<<sbox_logsize)]++;
    da2[(i^b) | ((sbox[i]^sbox[a])<<sbox_logsize)]--;
    da2[(i^b) | ((sbox[i]^sbox[b])<<sbox_logsize)]++;
  }
}

void compute_maxs_for_diff(out int max, out uint mult, int da1[]) 
{
  uint i;
  max = 0;
  mult = 0;
  for (i = 1; i < transform_size; i++) 
  {
    if (da1[i] > max) 
    {
      max = da1[i];
      mult = 1;
    } 
    else if (da1[i] == max) 
    {
      mult++;
    }
  }
}

/**
 * Computes the degree of each output bit of a S-box, seen as a multivariate polynomial of its 
 * input.
 *
 * Each output bit is interpolated by a polynomial of the input bits.
 * The degree of the polynomial is then computed. degrees[i] is the degree of the i-th output bit.
 */

void output_degrees(int degrees[], uint sbox[]) 
{
  int i, x, monome, r1, r2, d;
  static int fun_table[sbox_size];
  static int eval_order[sbox_size];
  static bool eval_order_init=false;
  int eval_fun(int x) 
  {
    int res = 0;
    int i;
    for (i = 0;i < sbox_size; i++)
    {
      if (((i&x)==i) && (fun_table[i]==1)) 
      {
        res = 1-res;
      }
    }
    return(res);
  }
  int monomial_degree(int x) 
  {
    int i;
    int res = 0;
    for (i = 0; i < sbox_logsize; i++) 
    {
      if ((x&1) == 1) 
      {
        res++;
      }
      x >>= 1;
    }
    return(res);
  }
  if (!eval_order_init) 
  {
    int current=0;
    for (d = 0; d <= sbox_logsize; d++) 
    {
      for (i = 0; i < sbox_size; i++) 
      {
        if (monomial_degree(i) == d)
        {
          eval_order[current++] = i;
        }
      }
    }
    /*writef("monomial by increasing degree:\n");
     for(i=0;i<sbox_size;i++) writef("%d ", eval_order[i]);
     writef("\n");*/
    eval_order_init = true;
  }
  //clear coefficients of the interpolating polynomial
  for (i = 0; i < sbox_logsize; i++) 
  {
    for (x = 0; x < sbox_size; x++)
    {
      fun_table[x] = 0;
    }
    for (x = 0; x < sbox_size; x++) 
    {
      monome = eval_order[x];
      r1 = ((sbox[monome])>>i) & 1;
      r2 = eval_fun(monome);
      if (r1<>r2) fun_table[monome]=1;
      assert(eval_fun(monome)==r1);
    }
    for (x = 0; x < sbox_size; x++) 
    {
      if (fun_table[eval_order[x]] <> 0) monome = eval_order[x];
    }
    degrees[i] = monomial_degree(monome);
  }
}


bool quadratic_equations(uint sbox[]) 
{
  static int inputs[sbox_size];
  const int linear=2*sbox_logsize;
  const int quadratic=sbox_logsize*(2*sbox_logsize-1);
  const int width=1+linear+quadratic;
  static  int[quadratic] quadratic_monomial;
  static bool[width] pivots;
  static int[width][sbox_size] matrix;
  static bool init=false;
  int i,j,k,l;
  int eval_monomial(int monomial, int input) 
  {
    if ((input&monomial)==monomial) return 1;
    else return 0;
  }
  void print_matrix() 
  {
    int i,j;
    for (i=0;i<sbox_size;i++) 
    {
      //writef("%8d  ",inputs[i]);
      for (j = 0; j < width; j++) 
      {
        writef("%d ",matrix[i][j]);
      }
      writef("\n");
    }
  }
  bool find_next_pivot(int start_row, out int row_idx, out int col_idx) 
  {
    int i, j;
    bool ans = false;
    for (i=start_row; (i<sbox_size) && (!ans); i++) 
    {
      for (j=0; (j<width) && (!ans);j++) 
      {
        if (!pivots[j] && matrix[i][j]!=0) 
        {
          ans=true;
          pivots[j]=true;
          row_idx=i;
          col_idx=j;
        }
      }
    }
    return(ans);
  }
  
  //once-init of the monomial indexing array
  if (!init) 
  {
    k = 0;
    for (i = 0; i < linear; i++) 
    {
      for (j = i + 1; j < linear; j++) 
      {
        quadratic_monomial[k++] = (1<<i)|(1<<j);
      }
    }
    init=true;
  }
  
  for (i = 0; i < sbox_size; i++) 
  {
    inputs[i] = i | (sbox[i]<<sbox_logsize);
  }
  for (j = 0; j < width; j++)
  {
    pivots[j] = false;
  }
  
  //matrix construction
  for (i = 0; i < sbox_size; i++) 
  {
    matrix[i][0] = 1;
  }
  for (i = 0; i < sbox_size; i++) 
  {
    for (j = 0; j < linear; j++) 
    {
      matrix[i][j+1] = (inputs[i]>>j)&1;
    }
  }
  for (i = 0; i < sbox_size; i++) 
  {
    for (j = 0; j < quadratic; j++) 
    {
      matrix[i][j + linear + 1] = eval_monomial(quadratic_monomial[j], inputs[i]);
    }
  }

  // gaussian reduction of the matrix
  int current_row = 0;
  bool finished = false;
  while (!finished)
  {
    if (find_next_pivot(current_row, i, j)) 
    {
      for (k = 0; k < width; k++) 
      {
        if (!pivots[k] && matrix[i][k] != 0) 
        {
          for (l = i; l < sbox_size; l++) 
          {
            matrix[l][k] ^= matrix[l][j];
          }
        }
      }
      current_row = i + 1;
      if (current_row >= sbox_size) finished = true;
    } 
    else
    {
      finished = true;
    }
  }
  
  //is the kernel trivial ?
  bool found_zero_column=false;
  bool is_zero;
  for (j = 0; j < width; j++) 
  {
    is_zero = true;
    for (i = 0; i < sbox_size; i++) 
    {
      if (matrix[i][j] != 0)
      {
        is_zero = false;
      }
    }
    if (is_zero)
    {
      found_zero_column = true;
    }
  }
  return found_zero_column;
}

uint num_fixed_points(uint[] sbox)
{
  uint count;
  for(uint i = 0; i < sbox_size; i++)
  {
    if(i == sbox[i]) 
    {
      count++;
    }
  }
  return count;
}

unittest{
  writeln("Entering hadamard unit test");
  int[] ha1 = new int[transform_size];
  int[] ha2 = new int[transform_size];
  uint[256] sbox;
  generate_random_sbox(sbox,Random(65));
  int max;
  uint mult;
  compute_maxlin2(max, mult, sbox, ha1);
  compute_maxlin(max, mult, sbox,ha2);
  if(ha1[1..$] != ha2[1..$])
  {
    void print_ar(int[] ha1, int[] ha2)
    {
      for(uint i = 0; i < 256;i++)
      {	
        if(ha1[i]!=ha2[i]) writef("!%2x/%2x", ha1[i], ha2[i]);
      }
      writefln("");
    }
    print_ar(ha1,ha2);
    print_ar(ha1[255*256 .. 256 * 256], ha2[255*256 .. 256 * 256]);
  }
  writeln("Exiting hadamard unit test");
}
