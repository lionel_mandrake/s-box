# README #
A S-box generator for symmetric block ciphers written in D. Generates S-boxes optimized against differential and linear cryptanalysis, and with no unexpected algebraic relation.

